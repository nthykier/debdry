#!/usr/bin/python3
# coding: utf8
#
# Copyright (C) 2014 Enrico Zini <enrico@enricozini.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging
import sys
from debdrylib.debdry import Debdry

parser = argparse.ArgumentParser(description='Merge manually and automatically generated debian/ packaging.')
parser.add_argument("dir", nargs="?", default=".", help="package source directory, default: %(default)s")
#parser.add_argument("--dry-run", action="store_true", help="")
parser.add_argument('--version', action='version', version='%(prog)s 0.1')
parser.add_argument('-v', '--verbose', action="store_true",
                    help="Verbose output")
parser.add_argument('--debug', action="store_true",
                    help="Debug output")
parser.add_argument('-r', '--restore', action="store_true",
                    help="Undebianise, leaving only debdry files in debian/")
parser.add_argument('--dry', action="store_true",
                    help="Take an existing package and add a minimal debian/debdry/")

args = parser.parse_args()

FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
if args.debug:
    logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=FORMAT)
elif args.verbose:
    logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)
else:
    logging.basicConfig(level=logging.WARN, stream=sys.stderr, format=FORMAT)


debdry = Debdry(args.dir, debug=args.debug)
if args.restore:
    debdry.restore()
elif args.dry:
    debdry.diff()
else:
    debdry.debianise()

