import unittest
from debdrylib.tree import UploadersControlField, PkglistControlField

class TestUploaders(unittest.TestCase):
    def _pfield(self, value):
        return UploadersControlField("Uploaders", value, False)
    def _sfield(self, value):
        return UploadersControlField("Uploaders", value, True)

    def test_plain(self):
        cf1 = self._pfield("Enrico Zini <enrico@enricozini.org>")
        cf2 = self._pfield("A <a@b.c>")
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(), "A <a@b.c>")

    def test_smart(self):
        cf1 = self._pfield("Enrico Zini <enrico@enricozini.org>")
        cf2 = self._sfield("A <a@b.c>")
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(), "Enrico Zini <enrico@enricozini.org>, A <a@b.c>")

        cf1 = self._pfield('"Adam C. Powell, IV" <hazelsct@debian.org>')
        cf2 = self._sfield("A <a@b.c>")
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(), '"Adam C. Powell, IV" <hazelsct@debian.org>, A <a@b.c>')

        cf1 = self._pfield('"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palazzi <palazziandrea@yahoo.it>')
        cf2 = self._sfield("A <a@b.c>")
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(),
                          '"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palazzi <palazziandrea@yahoo.it>, A <a@b.c>')

        cf1 = self._pfield('"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palazzi <palazziandrea@yahoo.it>')
        cf2 = self._sfield('"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palazzi <palazziandrea@yahoo.it>')
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(),
                          '"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palazzi <palazziandrea@yahoo.it>')

        cf1 = self._pfield('"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palozzi <palazziandrea@yahoo.it>')
        cf2 = self._sfield("Andrea Palazzi <palazziandrea@yahoo.it>, Enrico Zini <enrico@enricozini.org>")
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(),
                          '"Adam C. Powell, IV" <hazelsct@debian.org>, Andrea Palazzi <palazziandrea@yahoo.it>, Enrico Zini <enrico@enricozini.org>')

class TestDeps(unittest.TestCase):
    def _pfield(self, value):
        return PkglistControlField("Depends", value, False)
    def _sfield(self, value):
        return PkglistControlField("Depends", value, True)

    def assert_combine_plain(self, val1, val2, result):
        cf1 = self._pfield(val1)
        cf2 = self._pfield(val2)
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(), result)

    def assert_combine_smart(self, val1, val2, result):
        cf1 = self._pfield(val1)
        cf2 = self._sfield(val2)
        f = cf1.combine(cf2)
        self.assertEquals(f.to_string(), result)

    def test_plain(self):
        self.assert_combine_plain("foo", "bar", "bar")
        self.assert_combine_plain("foo (=1.4), bar (=1)", "bar", "bar")

    def test_deps(self):
        self.assert_combine_smart("foo", "bar", "foo, bar")
        self.assert_combine_smart("foo (=1.4), bar, baz (>> 1.0)",
                                  "foo (>= 1.5)",
                                  "foo (>= 1.5), bar, baz (>> 1.0)")
