#!/usr/bin/python3
# coding: utf8
#
# Copyright (C) 2014 Enrico Zini <enrico@enricozini.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os.path
import subprocess
import logging
import shutil

log = logging.getLogger(__name__)

class Auto:
    DEBIANIZERS = []

    def __init__(self, dirname):
        self.dirname = dirname

    def debianise(self):
        """
        Auto-build a debian/ directory based on the upstream packaging
        """
        raise NotImplementedError()

    def run_command(self, argv, **opts):
        """
        Runs a shell command
        """
        log.debug('$ ' + " ".join(argv))
        subprocess.check_call(argv, **opts)

    @classmethod
    def valid_for_source(cls, dirname):
        """
        Return true if this method thinks it can debianise the given source
        tree
        """
        return False

    @classmethod
    def instantiate(cls, dirname):
        """
        Instantiate a debianizer object for a source dir
        """
        valid = []
        for d in cls.DEBIANIZERS:
            if d.valid_for_source(dirname):
                valid.append(d)

        if not valid:
            # No valid debianizers were found
            for d in cls.DEBIANIZERS:
                label = getattr(d, 'LABEL', d.__class__.__name__)
                err_msg = getattr(d, 'FAILED_ATTEMPT_DESCRIPTION', 'not valid')
                log.info("%s: %s" % (label, err_msg))

            raise RuntimeError("No method found for debianising {}".format(dirname))

        if len(valid) == 1: return valid[0](dirname)
        raise RuntimeError("{} methods found debianising {}: {}".format(
            len(valid), dirname), ", ".join(str(x) for x in valid))

    @classmethod
    def debianise(cls, dirname):
        """
        Debianise the given directory autoselecting the method to use
        """
        debianiser = cls.instantiate(dirname)
        debianiser.debianise()

    @classmethod
    def register(cls, debianizer):
        """
        Register a class as an auto-debianizer.

        This can be used as a class decorator.
        """
        cls.DEBIANIZERS.append(debianizer)
        return debianizer

@Auto.register
class PerlDhMakePerl(Auto):
    LABEL = "dh-make-perl"
    FAILED_ATTEMPT_DESCRIPTION = "Makefile.PL and Build.PL not found"
    def debianise(self):
        self.run_command(["dh-make-perl", "--vcs", "none"], cwd=self.dirname)

    @classmethod
    def valid_for_source(cls, dirname):
        files = frozenset(os.listdir(dirname))
        return "Makefile.PL" in files or "Build.PL" in files

@Auto.register
class PythonStddeb(Auto):
    LABEL = "python"
    FAILED_ATTEMPT_DESCRIPTION = "setup.py not found"
    def debianise(self):
        # Keep track of the current directory contents, to detect cruft left
        # around by setup.py
        old_contents = set(os.listdir(self.dirname))

        with open(os.path.join(self.dirname, "setup.py"), "rt") as fd:
            if "python3" in next(fd):
                interpreter = "python3"
            else:
                interpreter = "python"
        self.run_command([interpreter, "setup.py", "--command-packages=stdeb.command", "debianize"], cwd=self.dirname)

        # Cleanup known cruft left around by setup.py
        new_contents = set(os.listdir(self.dirname))
        for f in new_contents - old_contents:
            if not f.endswith(".egg-info"): continue
            log.info("Cleaning setup.py cruft %s left in %s", f, self.dirname)
            shutil.rmtree(os.path.join(self.dirname, f))

    @classmethod
    def valid_for_source(cls, dirname):
        return os.path.exists(os.path.join(dirname, "setup.py"))

@Auto.register
class RubyDhMakeRuby(Auto):
    LABEL = "dh-make-ruby"
    FAILED_ATTEMPT_DESCRIPTION = "*.gemspec files not found"
    def debianise(self):
        self.run_command(["dh-make-ruby", "."], cwd=self.dirname)

    @classmethod
    def valid_for_source(cls, dirname):
        files = os.listdir(dirname)
        if any(x.endswith(".gemspec") for x in files): return True
        return "Gemfile" in files

@Auto.register
class HaskellCabalDebian(Auto):
    LABEL = "cabal-debian"
    FAILED_ATTEMPT_DESCRIPTION = "*.cabal files not found"
    def debianise(self):
        self.run_command(["cabal-debian"], cwd=self.dirname)

    @classmethod
    def valid_for_source(cls, dirname):
        files = os.listdir(dirname)
        return any(x.endswith(".cabal") for x in files)
